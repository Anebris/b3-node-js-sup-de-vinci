import { Request, Response } from 'express';
import { TaskCreation, TaskFinal } from '../types/task';
import * as taskService from '../services/task.service';

export const createTask = async (req: Request, res: Response) => {
  const task: TaskCreation = req.body;
  try {
    const newTask = await taskService.createTask(task);
    res.status(201).send(newTask);
  } catch (error) {
    res.status(500).send('Error creating task');
  }
}