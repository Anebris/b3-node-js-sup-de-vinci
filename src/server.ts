import express from 'express'
import routes from './routes/index'
import mongoose from 'mongoose'

import * as taskController from './controllers/task.controller'

const app = express()
const port = 3001

app.use(express.json())
app.use('/api', routes)

app.get('/', taskController.createTask)

mongoose.connect('mongodb+srv://kenandufrene:9rtqnFF7V8.qfZ9@cluster0.ydelekg.mongodb.net/?retryWrites=true&w=majority')
  .then(() => {
    console.log('mongodb est connecté')
    app.listen(port, () => {
      console.log(`Server is running on port ${port}`)
    })
  })
  .catch(() => {
    console.log('mongo est pas co !')
  })

