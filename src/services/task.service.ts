import { TaskCreation, TaskFinal } from "../types/task";
import * as taskRepository from '../repositories/task.respository'

export const createTask = async (task: TaskCreation) => {

  try {
    const tasks = await taskRepository.createTask(task)
    if (!tasks) throw new Error('Error creating task')

    /* const newLog = await logService.createLog({ feature: 'task', content: task})
    console.log(newLog); */
    
    return tasks
  } catch (error) {
    throw error
  }
}
