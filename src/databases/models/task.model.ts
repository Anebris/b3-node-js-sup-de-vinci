import mongoose from "mongoose";
import { TaskEpicSchema } from "../schemas/task.schema";

export const TaskEpicModel = mongoose.model('tasks', TaskEpicSchema)
// export const TaskClassiqueModel = mongoose.model('tasks', TaskClassiqueSchema)