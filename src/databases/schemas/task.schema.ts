import mongoose from "mongoose";

export const TaskEpicSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String
  },
  priority: {
    type: String,
    required: true,
    validate: {
      validator: (content) => {
        return ['low', 'medium', 'high'].includes(content)
      },
      message: props => `${props.value} n'a pas une priority valide`
    }
  }
})