export type TaskCreation = {
  title: string,
  priority: Number
}

export type TaskFinal = {
  id: Number,
  title: string,
  priority: Number
}