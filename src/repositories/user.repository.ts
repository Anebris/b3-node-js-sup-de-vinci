import { UserModel } from "../databases/models/user.model";

export const createUser = async (user) => {
  console.log('createUserRepo', user);
  
    try {
      const newUser = await UserModel.create(user);
      console.log(newUser);
      
      return newUser;
    }
    catch (error) {
      throw error;
    }
}

export const findUserByEmail = async (email) => {
  try {
    const user = await UserModel.findOne({ email })
    return user
  } catch (error) {
    throw error
  }
}