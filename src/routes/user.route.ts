import { Router } from 'express';
import * as userController from '../controllers/user.controller';
import { authMiddleware} from '../middlewares/auth.middleware';

const router = Router();

router.post('/', authMiddleware, userController.createUser);

export default router;