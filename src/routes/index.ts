import { Router } from 'express';
import taskRoutes from './task.route';
import authRoutes from './auth.route';
import userRoutes from './user.route';

const router = Router();

router.use('/v1/task', taskRoutes);
router.use('/v1/auth', authRoutes);
router.use('/v1/user', userRoutes);

export default router;